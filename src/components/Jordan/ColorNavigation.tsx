import { ArrowLeftIcon, ArrowRightIcon } from '@heroicons/react/16/solid'
import React from 'react'

function ColorNavigation() {
    return (
        <div className='flex flex-col gap-3 justify-end'>
            <div className='flex justify-center gap-3'>
                <div className={`rounded-full  bg-[#4f4d4d] w-2 h-2`}></div>
                <div className={`rounded-full shadow-md shadow-white bg-[red] w-2 h-2`}></div>
                <div className={`rounded-full bg-[#438fd9] w-2 h-2`}></div>
            </div>
            <div className='flex gap-10 text-white'>
                <div>
                    <p className="text-sm">Prev</p>
                    <ArrowLeftIcon/>
                </div>
                <div>
                    <p className="text-sm">Next</p>
                    <ArrowRightIcon/>
                </div>
            </div>
        </div>
    )
}

export default ColorNavigation
