import Image from 'next/image'
import React from 'react'

function BigPart() {
    return (
        <section className='text-white'>
            <h1 className='uppercase font-bold italic text-9xl'> Nike</h1>
            <h2 className='capitalize font-bold italic text-5xl mt-10 ml-16'> Air</h2>
            <h2 className='capitalize font-bold italic text-5xl ml-60 text-end'> Max 270</h2>
            <div className='absolute w-[390px] h-[185px] top-72'>
                <Image
                src={'/Images/chaussure-dunk-2.png'}
                fill
                className='object-cover -rotate-45'
                alt=''
                id='mainImg'
                />
            </div>
        </section>
    )
}

export default BigPart
