import React from 'react'

function SocialLink() {
    return (
        <div className='flex gap-10 text-white capitalize text-sm items-end'>
            <p>Facebook</p>
            <p>instagram</p>
        </div>
    )
}

export default SocialLink
