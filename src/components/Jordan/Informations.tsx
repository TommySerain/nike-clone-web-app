import React from 'react'

function Informations() {
    return (
        <div className='flex flex-col gap-3 font-medium text-white capitalize text-3xl'>
                <h3>{`Nike Air`}</h3>
                <h3>{`Max 270 punch`}</h3>
                <div className='flex gap-5 items-end'>
                    <h3> {`$115.99`} </h3>
                    <button className='text-sm'>add to cart +</button>
                </div>
        </div>
    )
}

export default Informations
