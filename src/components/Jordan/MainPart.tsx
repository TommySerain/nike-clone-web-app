import React from 'react'
import Progress from './Progress'
import BigPart from './BigPart'

function MainPart() {
    return (
        <section className='flex justify-between mt-32'>
            <Progress imgUrl = '/Images/chaussure-dunk.png' color='#4f4d4d' />
            <BigPart />
            <Progress imgUrl = '/Images/chaussure-dunk-3.png' color='#438fd9' />
        </section>
    )
}

export default MainPart
