import React from 'react'
import Informations from './Informations'
import SocialLink from './SocialLink'
import ColorNavigation from './ColorNavigation'

function BottomSection() {
    return (
        <section className='mt-10 flex justify-between'>
            <Informations />
            <SocialLink />
            <ColorNavigation />
        </section>
    )
}

export default BottomSection
