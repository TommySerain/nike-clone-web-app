import Image from 'next/image'
import React from 'react'

interface Props{
    imgUrl: string
    color: string
}

function Progress({imgUrl, color}:Props) {
    return (
        <section className='flex flex-col gap-5 items-center pt-16'>
            <div className='relative w-[78px] h-[37px]'>
                <Image
                src={imgUrl}
                fill
                className='object-cover w-7'
                alt=''
                />
            </div>
            <div className={`rounded-full bg-[${color}] w-2 h-2`}></div>
            <div className={`rounded-full w-[2px] h-[170px] bg-[${color}]`}></div>
        </section>
    )
}

export default Progress
