import React from 'react'
import VariantMainImage from './VariantMainImage'
import VariantInfo from './VariantInfo'
import { urlFor } from '@/sanity'

interface Props{
  variant: any
}

function VariantTopSection({variant}:Props) {
  return (
    <section
    className='flex w-full gap-10 p-14 justify-center'
    >
      <VariantMainImage
      imageUrl={variant?.mainImage ? urlFor(variant?.mainImage).url()! : `/Images/hado.jpg`}
      />
      <VariantInfo
      name={variant.product?.name}
      category={variant.product?.category?.name}
      description={variant.product?.description}
      price={variant.product?.price}
      color = {variant.color.value}
      />
    </section>
  )
}

export default VariantTopSection
