import Head from 'next/head'
import React from 'react'
import Navbar from './Navbar'


function Layout({children}:any) {
  return (
    <div 
    // #433e3e
    // bg-[linear-gradient(152deg,_#fff,_#00bfd8_42%,_#0083f5)]
    className={`w-full overflow-x-hidden flex flex-col flex-1 overflow-y-auto min-h-screen 
    font-body bg-[linear-gradient(0deg,_#000000,_#433e3e)] py-10 px-20
   `}>
      <Head>
      
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="keywords" content={'keywords'} />
        <meta name="description" content={'description'} />
        <meta charSet="utf-8" />
        <title>{'Nike'}</title>
        <link rel="icon" href="/Logos/Nike.png" />
      </Head>


      <Navbar/>

      <main
      className='flex flex-col bg-[linear-gradient(0deg,_#000000,_#433e3e)] flex-1 w-full overflow-x-hidden'
      >
        {children}
      </main>
    </div>
  
  )
}

export default Layout
