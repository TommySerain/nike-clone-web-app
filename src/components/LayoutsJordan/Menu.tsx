import { Squares2X2Icon } from '@heroicons/react/16/solid'
import Link from 'next/link'
import React from 'react'

function Menu() {
    return (
        <div className='flex gap-3 items-center'>
            <Link href={`#`}>
                <p className='capitalize'>men</p>
            </Link>
            <Squares2X2Icon className='w-7' />
        </div>
    )
}

export default Menu
