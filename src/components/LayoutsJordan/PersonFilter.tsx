import Link from 'next/link'
import React from 'react'

function PersonFilter() {
    return (
        <div className='flex gap-5'>
            <Link href={`#`}>
                <p className='capitalize'>men</p>
            </Link>
            <Link href={`#`}>
                <p className='capitalize'>women</p>
            </Link>
            <Link href={`#`}>
                <p className='capitalize'>kid</p>
            </Link>
        </div>
    )
}

export default PersonFilter
