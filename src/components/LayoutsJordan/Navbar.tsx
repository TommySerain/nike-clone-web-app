import React from 'react'
import PersonFilter from './PersonFilter'
import Search from './Search'
import Category from './Category'
import Menu from './Menu'
import Logo from './Logo'

function Navbar() {
    return (
        <nav className='flex justify-between bg-[#433e3e] text-white items-center'>
            <div className='w-1/3'>
                <Logo />
            </div>
            <PersonFilter />
            <Search />
            <div className='flex gap-10 items-center'>
                <Category />
                <Menu />
            </div>
        </nav>
    )
}

export default Navbar
