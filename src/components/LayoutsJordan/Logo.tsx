import Link from 'next/link'
import React from 'react'

function Logo() {
    return (
        <Link href={'/jordan'}
        className='text-white hover:text-[#757575] hover:underline'
        >
            <span className='font-bold text-lg'>
                {`River Island`}
            </span>
        </Link>
    )
}

export default Logo
