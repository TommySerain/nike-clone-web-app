import { MagnifyingGlassCircleIcon } from '@heroicons/react/24/outline'
import React from 'react'

function Search() {
    return (
        <MagnifyingGlassCircleIcon className='w-7' />
    )
}

export default Search
