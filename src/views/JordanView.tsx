import React from 'react'
import Layout from '../components/LayoutsJordan/Layout'
import Progress from '../components/Jordan/Progress'
import MainPart from '../components/Jordan/MainPart'
import BottomSection from '../components/Jordan/BottomSection'



function JordanView() {
    return (
        <Layout>
            <MainPart />
            <BottomSection />
        </Layout>
        
    )
}

export default JordanView
