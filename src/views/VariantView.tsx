import React from 'react'
import Layout from '../components/Layouts/Layout'
import VariantTopSection from '../components/Variants/VariantTopSection'

interface Props{
    variant:any
}

function VariantView({variant}:Props) {
    // console.log(variant)
    return (
        <Layout>
            <VariantTopSection
            variant={variant}/>
        </Layout>
    )
}

export default VariantView
