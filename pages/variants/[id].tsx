import { sanityClient } from '@/sanity'
import VariantView from '@/src/views/VariantView'
import { GetStaticProps } from 'next'
import React from 'react'

interface Props{
    variant: any
}

function variant({variant}:Props) {
    return (
        <VariantView variant={variant}/>
    )
}

export default variant

export async function getStaticPaths() {
    
    const query = `*[_type == "variant" && _id!=null]{
        _id
    } `;

    const variants = await sanityClient.fetch(query);
    
    const paths = variants
    .map((variant: any ) =>({
        params: { id: variant?._id }
        })
    )
    .flat();

    return {
        paths,
        fallback: false
    }
}

export const getStaticProps: GetStaticProps = async ({params}:any) => {
    
    const variantQuery= `*[_type == "variant" && _id == $id ][0] {
        _id,
        product->
        {
            _id,
            _createdAt,
            name,
            slug,
            mainImage,
            price,
            description,
            category->{
                name
            }
        },
        color->{
            value
        },
        mainImage,
        images
    }`

    const variant = await sanityClient.fetch(variantQuery, {
        id: params?.id,
    })

    if(!variant){
        return {
            notFound: true
        }
    }
    
    return {
    props: {
        variant,
    },
    revalidate: 10
    };
};
